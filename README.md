# **Configuración Software**

## Java.
* [JDK 1.8.0_251](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
* [JRE 1.8.0_251](https://www.oracle.com/java/technologies/javase-jre8-downloads.html)
## NetBeans.
* [8.2](https://netbeans.org/downloads/8.2/rc/)